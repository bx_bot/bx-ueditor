// sunxiaoran@baixing.com
const gulp = require('gulp')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')

gulp.task('bundle', () => gulp.src([
		'./ueditor.config.js',
		'./ueditor.all.js',
		'./lang/zh-cn/zh-cn.js',
	])
	.pipe(concat('ueditor.min.js', { newLine: '\n;' }))
	.pipe(uglify({
		preserveComments: 'some',
		compress: { screw_ie8: false },
		mangle: { screw_ie8: false },
		output: { screw_ie8: false },
	}))
	.pipe(gulp.dest('./'))
)
